//
//  Helpers.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import Foundation


struct Constants {
    struct Networking {
        static let apiEndpoint = /*"http://192.168.88.12:8080"// */"https://secure-shore-85352.herokuapp.com"
    }
    
    struct Navigation {
        static let login = "ShowLogin"
        static let initMain = "ShowMainFromInit"
        static let loginMain = "ShowMainFromLogin"
    }
    
    struct CellIdentifiers {
        static let MenuCell = "MenuCell"
    }
}
