//
//  MenuTableViewController.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import UIKit
import SDWebImage

class MenuTableViewController: UITableViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        MenuNetworking.shared.fetchMenuItems(token: UserNetworking.shared.currentUserToken)
        MenuNetworking.shared.didSetMenuItems = {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.onRefresh), for: .valueChanged)
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuNetworking.shared.menuItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.MenuCell, for: indexPath) as! MenuTableViewCell
        
        let menuItem = MenuNetworking.shared.menuItems[indexPath.row]
        
        cell.thumbnail.sd_setImage(with: URL(string: menuItem.imageURL), placeholderImage: #imageLiteral(resourceName: "MenuItemPlaceholder"))
        cell.title.text = menuItem.title
        cell.itemDescription.text = menuItem.description
        

        return cell
    }
    
    func onRefresh() {
        MenuNetworking.shared.fetchMenuItems(token: UserNetworking.shared.currentUserToken)
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let y = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom;
        let h = scrollView.contentSize.height;
        
        let reload_distance:CGFloat = 50;
        if(y > h + reload_distance) {
            MenuNetworking.shared.fetchMoreMenuItems(token: UserNetworking.shared.currentUserToken)
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        UserNetworking.shared.logout(fromController: self)
    }
    
}
