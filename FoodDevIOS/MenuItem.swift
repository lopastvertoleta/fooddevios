//
//  MenuItem.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import Foundation

class MenuItem {
    var title: String
    var description: String
    var imageURL: String
    
    
    init(title: String, description:String, image:String) {
        self.title = title
        self.description = description
        self.imageURL = image
    }
}
