//
//  MenuTableViewCell.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 12/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var itemDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        thumbnail.layer.masksToBounds = true
        thumbnail.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
