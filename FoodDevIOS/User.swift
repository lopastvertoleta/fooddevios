//
//  User.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    let id: Int
    let name: String
    let login: String
    let email: String
    let accessToken: String
    
    init(id: Int, name: String, login: String, email: String, accessToken: String) {
        self.id = id
        self.name = name
        self.login = login
        self.email = email
        self.accessToken = accessToken
    }
    
    init?(jsonData:Any) {
        let json = JSON(jsonData)
        guard let id = json["id"].int,
        let name = json["name"].string,
        let login = json["login"].string,
        let email = json["email"].string,
        let token = json["access_token"].string
        else { return nil }
        
        self.id = id
        self.name = name
        self.login = login
        self.email = email
        self.accessToken = token
    }
}
