//
//  UserNetworking.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON
import FBSDKLoginKit

class UserNetworking {
    private init() { }
    
    static let shared = UserNetworking()
    
    private var isLoggingIn = false {
        didSet {
            didSetIsLoggingIn?(isLoggingIn)
        }
    }
    var currentUser: User?
    var currentUserToken: String?
    var didSetIsLoggingIn: ((_ isLoggingIn: Bool) -> Void)?
    
    private func finishAuthentication(user: User) {
        
        self.currentUser = user
        self.currentUserToken = user.accessToken
        
        UserDefaults.standard.set(true, forKey: "authenticated")
        UserDefaults.standard.set(user.accessToken, forKey: "token")
    }
    
    func login(email: String, password: String) -> Promise<Void> {
        return Promise { fulfill, reject in
            self.isLoggingIn = true
            Alamofire.request(
                "\(Constants.Networking.apiEndpoint)/login",
                method: .post,
                parameters: ["email": email, "password": password],
                encoding: JSONEncoding.default
            )
            .responseJSON().then(execute: {[unowned self] responseJSON -> Void in
                guard let user = User(jsonData: responseJSON)
                    else {
                        throw UserNetworkingError(description: "Invalid user")
                }
                self.finishAuthentication(user: user)
                
                fulfill()
            }).catch(execute: { (error) in
                reject(UserNetworkingError(description: error.localizedDescription))
            }).always {
                self.isLoggingIn = false
            }
        }
    }
    
    func fetchCurrentUser(token: String) {
        Alamofire.request("\(Constants.Networking.apiEndpoint)/currentUser",
                    headers: ["Authorization": "Bearer \(token)"]
            ).responseJSON()
            .then(execute: {[unowned self] responseJSON -> Void in
                guard let user = User(jsonData: responseJSON) else {
                    throw UserNetworkingError(description: "Invalid user")
                }
                self.currentUser = user
            })
            .catch { (error) in
                print(error)
        }
    }
    
    func login(facebookToken: String) -> Promise<Void> {
        return Promise(resolvers: { (fulfill, reject) in
            self.isLoggingIn = true
            Alamofire.request(
                "\(Constants.Networking.apiEndpoint)/facebookLogin",
                method: .post,
                parameters: ["token": facebookToken],
                encoding: JSONEncoding.default)
                .responseJSON()
                .then(execute: { (responseJSON) -> Void in
                    guard let user = User(jsonData: responseJSON)
                        else {
                            throw UserNetworkingError(description: "Invalid user")
                    }
                    self.finishAuthentication(user: user)
                    
                    fulfill()
                })
                .catch(execute: { (error) in
                    reject(error)
                })
                .always {
                    self.isLoggingIn = false
            }
        })
    }
    
    func logout(fromController controller: UIViewController) {
        UserDefaults.standard.set(false, forKey: "authenticated")
        UserDefaults.standard.set(nil, forKey: "token")
        let login = controller.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        UIApplication.shared.keyWindow?.rootViewController = login
    }
    
    class UserNetworkingError: Error {
        let localizedDescription: String
        
        init(description: String) {
            localizedDescription = description
        }
    }
}

















