//
//  ViewController.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController {
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var goToEmailLoginButton: UIButton!
    @IBOutlet weak var emailLoginView: UIView!
    @IBOutlet weak var hideEmailLoginViewButton: UIButton!

    @IBOutlet weak var mainLoginTrailing: NSLayoutConstraint!
    @IBOutlet weak var mainLoginViewLeading: NSLayoutConstraint!
    @IBOutlet weak var emailLoginBottom: NSLayoutConstraint!
    
    let facebookLoginManager = FBSDKLoginManager()
    var loadingView: LoadingView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureViews()
        configureButtons()
        UserNetworking.shared.didSetIsLoggingIn = {
            isLoggingIn -> Void in
            if isLoggingIn {
                self.loadingView = LoadingView.showIn(superView: self.view)
            } else {
                self.loadingView?.remove()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK:- ACTIONS
    @IBAction func goToEmailLogin(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.emailLoginBottom.constant = 0 - self.emailLoginView.layer.cornerRadius
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func hideEmailLogin(_ sender: Any) {
        UIView.animate(withDuration: 0.5) { 
            self.emailLoginBottom.constant = -self.emailLoginView.frame.height
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func login(_ sender: Any) {
        UserNetworking.shared.login(email: self.email.text ?? "", password: self.password.text ?? "")
            .then {[unowned self] _ -> Void in
                self.performSegue(withIdentifier: Constants.Navigation.loginMain, sender: self)
            }
            .catch(execute: showErrorAlert)
    }
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        facebookLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            guard let result = result, let token = result.token, let tokenString = token.tokenString else { return }
            
            UserNetworking.shared.login(facebookToken: tokenString)
                .then { _ -> Void in
                    self.performSegue(withIdentifier: Constants.Navigation.loginMain, sender: self)
                }
                .catch(execute: self.showErrorAlert)
        }
    }
    

//MARK:- HELPERS
    func configureViews() {
        emailLoginBottom.constant = -emailLoginView.frame.height
        
        emailLoginView.layer.cornerRadius = emailLoginView.frame.height / 8
        emailLoginView.layer.borderColor = UIColor.black.cgColor
        emailLoginView.layer.borderWidth = 1.0
    }
    
    func configureButtons() {
        facebookButton.layer.cornerRadius = facebookButton.frame.height / 4
        goToEmailLoginButton.layer.cornerRadius = goToEmailLoginButton.frame.height / 4
        hideEmailLoginViewButton.imageView?.contentMode = .scaleAspectFit
    }
    
    func showErrorAlert(error: Error) {
        let alertController = UIAlertController(
            title: "Authentication error" ,
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}














