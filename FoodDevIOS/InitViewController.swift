//
//  InitViewController.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import UIKit

class InitViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        let authenticated = UserDefaults.standard.bool(forKey: "authenticated")
        let token = UserDefaults.standard.string(forKey: "token")
        if authenticated, let token = token {
            UserNetworking.shared.currentUserToken = token
            UserNetworking.shared.fetchCurrentUser(token: token)
            performSegue(withIdentifier: Constants.Navigation.initMain, sender: self)
        } else {
            performSegue(withIdentifier: Constants.Navigation.login, sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
