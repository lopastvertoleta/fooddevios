//
//  MenuNetworking.swift
//  FoodDevIOS
//
//  Created by Taras Parkhomenko on 11/02/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MenuNetworking {
    private init() { }
    //MARK:- PROPERTIES
    //MARK: Singleton
    static let shared = MenuNetworking()
    
    //MARK: Pagination
    private let pageSize = 10
    private var page: Int {
        get {
            return menuItems.count > 0 ? Int(ceil(Double(menuItems.count / pageSize))) : 1
        }
    }
    private var limit: Int {
        get {
            return page * pageSize
        }
    }
    private var offset: Int {
        get {
            return page - 1 > 0 ? (page - 1) * pageSize : pageSize
        }
    }
    
    private var alreadyUpdating = false
    
    private var needMore: Bool {
        get {
            return menuItems.count % pageSize == 0 && !alreadyUpdating
        }
    }
    
    //MARK: Dynamiс menu items
    var didSetMenuItems: (() -> Void)?
    
    var menuItems = [MenuItem]() {
        didSet { didSetMenuItems?() }
    }
    
    //MARK:- METHODS
    func fetchMenuItems(token: String?) {
        guard let token = token else { return }
        Alamofire.request(
            "\(Constants.Networking.apiEndpoint)/menuItems",
            parameters: ["limit": 10, "offset": 0],
            headers: ["Authorization": "Bearer \(token)"]
        )
        .responseJSON()
        .then {[unowned self] responseJSON -> Void in
            print(JSON(responseJSON))
            let json = JSON(responseJSON).arrayValue
            
            self.menuItems = json.reduce([], { (result, node) -> [MenuItem] in
                
                guard
                let title = node["title"].string,
                let description = node["description"].string,
                let imageURL = node["imageURL"].string
                    else { return result }
                return result + [ MenuItem(title: title, description: description, image: imageURL) ]
            })
        }
        .catch { (error) in
            print(error)
        }
    }
    
    func fetchMoreMenuItems(token: String?) {
        guard let token = token, needMore else { return }
        
        alreadyUpdating = true
        
        Alamofire.request(
            "\(Constants.Networking.apiEndpoint)/menuItems",
            parameters: ["limit": limit, "offset": offset],
            headers: ["Authorization": "Bearer \(token)"]
            )
            .responseJSON()
            .then {[unowned self] responseJSON -> Void in
                let json = JSON(responseJSON).arrayValue
                
                self.menuItems = json.reduce(self.menuItems, { (result, node) -> [MenuItem] in
                    guard
                        let title = node["title"].string,
                        let description = node["description"].string,
                        let imageURL = node["imageURL"].string
                        else { return result }
                    return result + [ MenuItem(title: title, description: description, image: imageURL) ]
                })
            }
            .catch { (error) in
                print(error)
            }
            .always {
                self.alreadyUpdating = false
        }
    }
}





