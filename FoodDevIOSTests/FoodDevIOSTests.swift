//
//  FoodDevIOSTests.swift
//  FoodDevIOSTests
//
//  Created by Taras Parkhomenko on 25/07/2017.
//  Copyright © 2017 Taras Parkhomenko. All rights reserved.
//

import XCTest
@testable import FoodDevIOS

class FoodDevIOSUserTests: XCTestCase {
    
    let mockUserJSON = "{  \"access_token\": \"a3f854a794ff3a03c94bea4ff85481484580a536f730e799f56b41f7227d743e\", \"email\": \"sovavgostinoy@gmail.com\",\"id\": 1,\"login\": \"lopastvertoleta\",\"name\": \"Taras Parkhomenko\" }".data(using: .utf8)!
    
    let wrongMockUserJSON = "{ \"id\": 1,\"login\": \"lopastvertoleta\" }".data(using: .utf8)!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCorrectUserParsing() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let user = User(jsonData:mockUserJSON)
        
        XCTAssert(user != nil)
    }
    
    func testWrongUserParsing() {
        let user = User(jsonData: wrongMockUserJSON)
        
        XCTAssert(user == nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
